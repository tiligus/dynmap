#!/bin/bash
#echo "here i am: $(pwd)" # print location
TFILE=/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh
if test -f "$TFILE"; then
    echo "Plugin already installed." && exit
fi
sudo bash nginx_builder_install.sh # install nginx builder
echo "location /map/ {
            rewrite ^/map/(.*) /\$1 break;
            proxy_pass http://localhost:8123;
            proxy_set_header X-Real-IP \$remote_addr;
            proxy_set_header Host \$host\$uri;
            proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
            proxy_set_header X-NginX-Proxy true;
            proxy_redirect off;
            proxy_connect_timeout 10;
       }" | sudo tee -a /etc/nginx/builder/map.conf # create dynmap config for webserver
sudo /etc/nginx/builder/make_config.sh # build new nginx config
cp Dynmap-3.3-beta-1-spigot.jar /home/ubuntu/mc-server/plugins/Dynmap-3.3-beta-1-spigot.jar # copy plugin to plugin directory
sudo mkdir /home/ubuntu/.scripts/plugins && sudo chmod 777 /home/ubuntu/.scripts/plugins
curl -H "Content-type: application/json" -d '{"add_record":true, "name":"Dynmap 3.3-beta-1","remove_script":"/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh","file_0":"dynmap/configuration.txt","file_1":"dynmap/lightings.txt","file_2":"dynmap/markers.yml","file_3":"dynmap/perspectives.txt","file_4":"dynmap/shaders.txt","file_5":"dynmap/shaders.txt","file_6":"dynmap/worlds.txt"}' 'http://127.0.0.1:8181' # add plugin to panel
echo "#!/bin/bash">/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh
#echo "rm -rf /home/ubuntu/mc-server/plugins/dynmap">>/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # delete plugin folder
echo "rm /home/ubuntu/mc-server/plugins/Dynmap-3.3-beta-1-spigot.jar">>/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # delete plugin
echo "sudo rm /etc/nginx/builder/map.conf && sudo /etc/nginx/builder/make_config.sh">>/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # delete nginx location conf
echo "curl -H \"Content-type: application/json\" -d '{\"remove\":true, \"name\":\"Dynmap 3.3-beta-1\"}' 'http://127.0.0.1:8181'">>/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # delete plugin from panel
echo "rm /home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh && echo Removal went fine">>/home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # delete plugin remover & remove message
chmod +x /home/ubuntu/.scripts/plugins/rmdynmap-3.2-beta-3-spigot.sh # make it executable
echo "Plugin installation went fine"
