#!/bin/bash
#DEFAULT NGINX CONFIG BUILDER BY TILIGUS
sudo mkdir /etc/nginx/builder
FILE=/etc/nginx/builder/init.txt
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else
    echo "$FILE does not exist."
echo "##
# You should look at the following URL\'s in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# https://www.nginx.com/resources/wiki/start/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/
# https://wiki.debian.org/Nginx/DirectoryStructure
#
# In most cases, administrators will remove this file from sites-enabled/ and
# leave it as reference inside of sites-available where it will continue to be
# updated by the nginx packaging team.
#
# This file will automatically load configuration files provided by other
# applications, such as Drupal or Wordpress. These applications will be made
# available underneath a path with that package name, such as /drupal8.
#
# Please see /usr/share/doc/nginx-doc/examples/ for more detailed examples.
##

# Default server configuration
#
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;

        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;

        server_name _;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files \$uri \$uri/ =404;
        }
        location /panel {
                rewrite /panel/(.*) /\$1  break;
                proxy_redirect     off;
                proxy_set_header   Host \$host;
                proxy_pass http://127.0.0.1:8080/;
                proxy_pass_request_headers on;
        }

        location /panel/assets {
                alias /home/ubuntu/.nodecpl/assets/;
                index index.html
                expires 1y;
                add_header Cache-Control \"public, no-transform\";
                try_files \$uri \$uri/ =404;
        }

        location /panel/api/upload {
        rewrite /panel/(.*) /\$1  break;
                proxy_redirect     off;
                proxy_set_header   Host \$host;
                proxy_pass http://127.0.0.1:9276/;
                proxy_pass_request_headers on;
                client_max_body_size 195M;
                client_body_timeout 1200;
        }

        location /socket_xWE87lOjk/ {
                proxy_http_version 1.1;

                proxy_set_header Upgrade \$http_upgrade;
                proxy_set_header Connection \"Upgrade\";

                proxy_pass \"http://localhost:8007/\";
        }">/etc/nginx/builder/init.txt
fi
FILE2=/etc/nginx/builder/end.txt
if [ -f "$FILE2" ]; then
    echo "$FILE2 exists."
else
    echo "$FILE2 does not exist."
    echo "}">/etc/nginx/builder/end.txt
fi
FILE3=/etc/nginx/builder/make_config.sh
if [ -f "$FILE3" ]; then
    echo "$FILE3 exists."
else
    echo "$FILE3 does not exist."
    echo "#!/bin/bash
cd /etc/nginx/builder
FILES=\"*.conf\"
for f in \$FILES
do
# FAILSAFE #
  if [ -f \"\$f\" ]
  then
    echo \"Processing \$f file...\"
    LOCATIONS=\$LOCATIONS\`cat \"\$f\"\`
  else
    echo \"Warning: Some problem with \\"\"\$f\\"\"\"
  fi
done
INIT=\`cat init.txt\`
END=\`cat end.txt\`
echo \"\$INIT\$LOCATIONS\$END\">/etc/nginx/sites-enabled/default
killall -9 nginx">/etc/nginx/builder/make_config.sh
chmod +x /etc/nginx/builder/make_config.sh
fi
